using Distributed, SharedArrays
import Statistics: mean, cov, var
import LinearAlgebra.norm

@everywhere using Metaheuristics, CEC17

@everywhere const desired_accu = 1e-4
@everywhere const D = 10
@everywhere const max_NFEs = 1000*D

@everywhere function accuracy_termination(P::Array)
    sol = Metaheuristics.getBest(P, :minimize)

     return abs(sol.f) < desired_accu

end

function lowerLevel(A, Φ, i, D)
        NFEs = SharedArray{Float64}(nprocs())
        NRUNS = 10 ###############################

        f(x) = begin
            NFEs[myid()] += 1
            acc = abs(cec17_test_func(x, i) - 100i)
            return acc < desired_accu ? 0.0 : acc
        end

        errors = SharedArray{Float64}(NRUNS)

        @sync @distributed for r = 1:NRUNS
            errors[r] = A(Φ, f)
        end


        # measures
        μ = Float64[
            mean(errors),
            var(errors),
            abs(maximum(errors) - minimum(errors)),
            sum(NFEs) / (max_NFEs*NRUNS)
        ]

        println(μ)
        return μ
end

function BCA(F::Function, Ψ::Function, bounds_ul)
    D_ul = size(bounds_ul,2)
    K = 7
    N = K*D_ul


    Fobj(Φ) = begin
        # y ∈ arg min { f(x, z) : z ∈ Y }
        μ = Ψ(Φ)

        # return upper level value
        return F(Φ, μ)
    end

    # optimize
    best, Population, t, upper_nevals = eca(Fobj, D_ul;
                    limits=bounds_ul,
                    K = K,
                    N = N,
                    showResults=false,
                    p_bin = 0,
                    p_exploit = 2,
                    max_evals=200D_ul,
                    termination = accuracy_termination,
                    returnDetails = true,
                    canResizePop=true,
                    showIter=true)

    μ = Ψ(best.x)

    return Population #best.x, best.f
    

end


@everywhere function main()
    F(Φ, μ) = sum(μ) #+ norm( cov(μ * μ') )

    # Algorithm A (with parameters Φ) solving problem f 
    A(Φ, f) = begin
        x, fx = DE(f, D; F = Φ[1], CR = Φ[2], N = round(Int, Φ[3]), max_evals=max_NFEs, termination=accuracy_termination, showResults=false)
        fx
    end

    fnum = 1
    Ψ(Φ) = lowerLevel(A, Φ, fnum, D)
    bounds_ul = [0 0 D; 2 1.0 20D]

    BCA(F, Ψ, bounds_ul)

end

# main()