import Metaheuristics
const mh = Metaheuristics
import Random: randperm
using BiApprox
using CEC17
import Statistics: mean, var
import LinearAlgebra.det

using ScikitLearn
if !@isdefined(KNeighborsRegressor)
    @sk_import neighbors: KNeighborsRegressor
end

function knn_train(X, Fs, k)
    neigh = KNeighborsRegressor(n_neighbors=k)
    fit!(neigh, X, Fs)
    return neigh
end

F_knn(x, model)  = predict(model, Array(x'))[1]


function replaceWorst(Population, Population_approx, best, Fobj, Fapprox)
    #########################################
    #########################################
    #   Selection
    #########################################
    #########################################

    if length(Population_approx) == 0
        return Population, best
    end

    sort!(Population, lt = (a, b) -> a.f > b.f )
    sort!(Population_approx, lt = (a, b) -> a.f < b.f )
    
    i = 1

    flag = 0
    flag2 = 0
    for new_sol in Population_approx[1:min(1, length(Population_approx))]
        
        if new_sol.f > Fapprox(Population[i].x)
            continue
        end

        Fh = Fobj(new_sol.x)
        flag2 += 1
        if Population[i].f > Fh
            Population[i] = mh.generateChild(new_sol.x, Fh)

            flag += 1

            if mh.Selection(best, Population[i], :minimize)
                best = Population[i]
                break
            end
            i+=1
        end
        
    end
    #########################################
    #########################################

        println(">>>>>  ", flag, " ", flag2, " >>    ", flag / flag2, " best ", best.f)

    return Population, best
end

function Iteration(F, Population, best, K, η_max, bounds, desired_accur, max_local_iters; searchType=:minimize)
    N = length(Population)
    a, b = bounds[1,:], bounds[2,:]
    stop = false

    to_evaluate = zeros(Int, N)

    for t = 1:max_local_iters
        I = randperm(N)
        for i in 1:N
            # current
            x = Population[i].x

            # generate U masses
            U = mh.getU(Population, K, I, i, N)
            
            # generate center of mass
            c, u_worst, u_best = mh.center(U, searchType)

            # stepsize
            η = η_max * rand()

            # u: worst element in U
            u = U[u_worst].x
            
            # current-to-center/bin
            h = x + η * (c - u)
            
            h = mh.correct(h, a, b, true)

            sol = mh.generateChild(h, F(h))

            if sol.f < 0
                continue
            end

            # replace worst element
            if mh.Selection(Population[i], sol, searchType)
                id_worst = mh.getWorstInd(Population, searchType)

                to_evaluate[id_worst] = 1
                Population[id_worst] = sol

                if mh.Selection(best, sol, searchType)
                    best = sol
                    stop = abs(best.f) < desired_accur
                    stop && break
                end
            end
            
        end
        
        stop && break
    end


    return Population, best, stop, to_evaluate

end

function get_training_set(Population, N_train, D)
    N_train = length(Population)
    Population_local = deepcopy(Population)
    X_train = zeros(N_train, D)
    Fs = zeros(N_train)
    
    i = 1
    for sol = Population_local
        X_train[i, :] = sol.x
        Fs[i] = sol.f
        i += 1
    end

    ###################################
    # Train model
    ###################################
    model = knn_train(X_train, Fs, 3)
    # α = zeros(N_train)
    # try
    #     α = train_model(Fs, X_train, zeros(2,2))
    # catch
    #     @warn "pasa algo..."
    # end
    ###################################

    return Population_local, X_train, Fs, model
end

function BCA(F::Function, Ψ::Function, bounds::Array)
    # general parameters
    searchType = :minimize
    η_max = 2.0
    K = 7
    D = size(bounds, 2)
    N = 2K*D
    # bounds = [upper_bounds lower_bounds]
    max_evals = 300
    max_iters = 10*(div(max_evals, N) + 1)
    desired_accur = 1e-5
    use_approx = false
    max_local_iters = 10
    #############################

    # current evaluations
    nevals = 0

    Fobj(Φ) = begin
        # y ∈ arg min { f(x, z) : z ∈ Y }
        μ = Ψ(Φ)
        nevals += 1
        # return upper level value
        return F(Φ, μ)
    end

    a, b = bounds[1,:], bounds[2,:]

    # initialize population
    Population = mh.initializePop(Fobj, N, D, a, b, :uniform)

    # stop condition
    stop = false

    # current generation
    t = 0

    # best solution
    best = mh.getBest(Population, searchType)
  
    N_train = N_train_max = N #2D
    N_train_min = 2D
    X_train = zeros(N_train, D)
    Fs = zeros(N_train)
    α  = zeros(N_train)
    ratio_max = Inf
    ratio = 1
    # start search
    while !stop
        I = randperm(N)

        Population_local, X_train, Fs, model = get_training_set(Population, N_train, D)

        Fapprox(x) = F_knn(x, model)
        ###################################

        if t == 0
            ratio_max = mean(var(X_train, dims=1))
        end
        
        ratio = mean(var(X_train, dims=1)) 

        if ratio > ratio_max
            ratio_max = ratio#mean(var(X_train, dims=1))
        end

        ratio /= ratio_max


        #########################################
        #########################################
        # Main iterations
        #########################################
        #########################################
        Population_approx, best_approx, stop_approx, to_evaluate = Iteration(Fapprox, Population_local, best, K, η_max, bounds, -1, max_local_iters)
        Population, best  = replaceWorst(Population, Population_approx[to_evaluate.==1], best, Fobj, Fapprox)
        Population, best, stop, _ = Iteration(Fobj, Population, best, K, η_max, bounds, desired_accur, 1)


        
        println("iter : $t \t evals: $nevals \t Error: ", best.f)

        stop && break
        
        # println(2t, " evals ", nevals, " ", best.f)
        #########################################
        #########################################

        t += 1


        N_train = N_train_min + round(Int, ( N_train_max - N_train_min ) * ratio)
        #######################################
        #######################################
        # STOP
        #######################################
        #######################################
        stop = abs(best.f) < desired_accur
        stop = stop || nevals >= max_evals
        stop = stop || t >= max_iters
        stop && break
        #######################################
        #######################################

    end

    if t <= 2
        display(X_train)
        display(Population)
    end

    println("iters: ", t)
    println("evals: ", nevals)
    return best
end

function test()
    # F(x,y) =  cec17_test_func(x, 1) - 100 #sum(x.^2 - 10cos.(2π*x)) + 10length(x)  
    # D = 10
    # bounds      = 100ones(2, D)
    # bounds[1,:] = -bounds[1,:]

    Ferr(x) = mean(x) + var(x) + abs(minimum(x) - minimum(x))
    F(x, y) = Ferr( abs.((0.01x[1]).^2 .+ x[2].*randn(31)) )
    Ψ(x) = 2x

    bounds = [0 0; 10 10.0]
    BCA(F, Ψ, bounds)
end

test()